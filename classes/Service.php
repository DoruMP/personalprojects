<?php


class Service extends Product
{

    public $name;

    public $percent;

    public $categ_id;

    public static function getTableName()
    {
        return 'services';
    }

    public function getPrice($parent=null)
    {
        return $parent->getPrice() * $this->percent / 100;
    }

    public function getProdServices()
    {
        return ProductService::findBy('product_id', $this->getId());
    }

    /**
     * @return mixed
     */
    public function getName($parent=null)
    {
        $parentName = $parent->getName();  //Product::find($this->id)->getName();
        return $this->name."($parentName)";
    }
}