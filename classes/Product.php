<?php


class Product extends BaseClass
{
    public $name;

    public $price;

    public $discount;

    public $availability;

    public $categ_id;

    public $seller_id;

    public function getSeller()
    {
        return User::find($this->seller_id);
    }

    public function getCategory()
    {
        return Category::find($this->categ_id);
    }

    public function getProdImages()
    {
        return ProdImage::findBy('prod_id', $this->getId());
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function getdiscount()
    {
        if ($this->getCategory()->name == 'Telefon'){

            return 20;
        }
        return $this->discount;
    }
    public function getFinalPrice()
    {
        return $this->getPrice()-($this->getPrice() * $this->getdiscount()/100);
    }

    public static function getTableName()
    {
        return 'products';
    }

    /**
    * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return
     */
    public function getProductServices()
    {
        return ProductService::findBy('product_id', $this->getId());
    }

}