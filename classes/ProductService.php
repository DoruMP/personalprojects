<?php


class ProductService extends BaseClass
{

    public $product_id;

    public $service_id;


    public static function getTableName()
    {
        return 'prod_services';
    }

    public function getService()
    {
        return Service::find($this->service_id);
    }

}