<?php
include "classes/BaseClass.php";
include "classes/User.php";
include "classes/Category.php";
include "classes/Product.php";
include "classes/Availability.php";
include "classes/ProdImage.php";
include "classes/ProductService.php";
include "classes/Service.php";

$salt = 'Mys3cr3s@alt';
session_start();

$conn = mysqli_connect('127.0.0.1', "root", 'Sco@l@it123', "national-01-doru");

function runQuery($sql){
    global $conn;
    $query = mysqli_query($conn, $sql);

    if (!$query){
        die("Mysql error on query:$sql - ".mysqli_error($conn));
    }
    if (is_bool($query)){
        return mysqli_insert_id($conn);
    } else {
        return $query->fetch_all(MYSQLI_ASSOC);
    }
}

//function displayTableData($data)
//{
//    $columns = array_keys($data[0]);
//    ?>
<!--    <table class="table">-->
<!--        <tr>-->
<!--            --><?php //foreach ($columns as $column): ?>
<!--                <th>--><?php //echo ucfirst($column); ?><!--</th>-->
<!--            --><?php //endforeach; ?>
<!--        </tr>-->
<!---->
<!--        --><?php //foreach ($data as $line): ?>
<!--            <tr>-->
<!--                --><?php //foreach ($line as $value): ?>
<!--                    <td>--><?php //echo $value; ?><!--</td>-->
<!--                --><?php //endforeach; ?>
<!--            </tr>-->
<!--        --><?php //endforeach; ?>
<!--    </table>-->
<!--    --><?php
//}

function getDecimalPrice ($newPrice) {
    return ($newPrice*100)%100;
}

function calculateAverage ($average, $arrayCategorie)
{
    foreach ($arrayCategorie as $item => $review) {
        $average+=$arrayCategorie[$item][2]/(count($arrayCategorie)-1);
    }
    return round($average,2);
}

function checkLogin(){
    if(!isset($_SESSION['seller_id'])){
        header('Location: index.php');
        die;
    }

}

?>